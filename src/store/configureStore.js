import thunk from 'redux-thunk';
import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import authReducer from '../reducers/auth';
import reportReducer from '../reducers/report';
import ambulanceReducer from '../reducers/ambulance';

const composeEnhacers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const store = createStore(
        combineReducers({
            auth: authReducer,
            report: reportReducer,
            ambulance: ambulanceReducer
        }),
        composeEnhacers(applyMiddleware(thunk))
    )
    return store;
}

