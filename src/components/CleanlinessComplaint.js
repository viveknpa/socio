import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';
import { history } from '../routers/AppRouter';
const _ = require("lodash");
import PlacesSearchBox from './maps/PlacesSearchBox';
import MapWithControlledZoom from './maps/MapWithControlledZoom';

class CleanlinessComplaint extends React.Component {
  state = {
    lat: undefined,
    lng: undefined,
    address: '',
    isMarkerShown: false
  }
  submitLocation = ({ latitude: lat, longitude: lng }) => {
    this.setState({ lat, lng });
  }
  handleCurrentLocation = () => {
    if(!navigator.geolocation) {
        return alert('geolocation not supported by your browser');
    }
    var geoOptions = {
        enableHighAccuracy: true
    }
    var geoError = (error) => {
        console.log('Error occurred. Error code: ' + error.code);
        // error.code can be:
        //   0: unknown error
        //   1: permission denied
        //   2: position unavailable (error response from location provider)
        //   3: timed out
    };
      navigator.geolocation.getCurrentPosition((position) => {
        this.setState({ 
          lat: position.coords.latitude, 
          lng: position.coords.longitude
         })
      }, geoError, geoOptions);
  }
  onSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    formData.append('latitude', this.state.lat);
    formData.append('longitude', this.state.lng);
    axios({
      url: `/post`,
      method: 'POST',
      data: formData,
      headers: {
        'Content-Type': 'multipart/form-data',
        'x-auth': localStorage.getItem('x-auth')
      }
    }).then((response) => {
      history.push('/dashboard/viewPost');
    }).catch((e) => {
      console.log(e);
    })
  }
  render() {
    const location = { lat: this.state.lat, lng: this.state.lng };
    return (
        <Form onSubmit={this.onSubmit} style={{padding: '20px'}}>
            <FormGroup>
              <Label for="subject">Title</Label>
              <Input type="textarea" name="subject" id="exampleText" />
            </FormGroup>
            <FormGroup style={{maxWidth: '396px', display: 'flex', flexDirection: 'column'}}>
              <Label for="location">Pin location</Label>
              <PlacesSearchBox
                submitLocation={this.submitLocation}
                placeholder="Search places"
              />
              <span style={{textAlign: 'center'}}>OR</span>
              <Button outline color="info" type="button" onClick={this.handleCurrentLocation}>Get current Location</Button><br />
              { this.state.lat &&  <MapWithControlledZoom location={location} /> }
            </FormGroup>
            <FormGroup>
              <Label for="images">Upload images</Label>
              <Input type="file" name="images" id="exampleFile" multiple />
            </FormGroup>

            <Button color="primary">Submit</Button>
        </Form>
    )
  }
}

export default CleanlinessComplaint;