import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Input, FormText, Alert } from "reactstrap";

export default class LoginForm extends React.Component {
    onLoginSubmit = (e) => {
        e.preventDefault();
        this.props.startLogin({
            email: e.target.email.value,
            password: e.target.password.value
        });
    }
    render() {
        return (
            <div>
                <Form onSubmit={this.onLoginSubmit}>
                    <h1>Log in to Logo</h1>
                    {this.props.loginError && <Alert color="danger"> {this.props.loginError} </Alert>}
                    <FormGroup>
                        <Input
                            type="email"
                            name="email"
                            placeholder="email" />
                    </FormGroup>
                    <FormGroup>
                        <Input
                            type="password"
                            name="password"
                            placeholder="password" />
                    </FormGroup>
                    <div>
                        <Button style={{ background: 'rgb(7, 71, 166)', marginRight: '20px' }}>Log In</Button>
                        <Link to="/forgot" style={{ }}>forgot password ?<br/></Link>
                    </div>
                    <span className="show-for-mobile">Need an account?
                    <Button
                        onClick={this.props.handleFormDisplay}
                        color="link"
                        style={{ paddingLeft: '0px', paddingTop: '0' }}
                    >
                        Sign Up
                    </Button>
                    </span>
                </Form>
            </div>
        )
    }
}