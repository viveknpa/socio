import React from 'react';
import axios from 'axios';
import { Input, Button, Modal, ModalHeader, ModalBody, ModalFooter, UncontrolledCarousel} from 'reactstrap';
import { Link } from 'react-router-dom';
import Comment from './comment';
import moment from 'moment';

class PostListItem extends React.Component {
    state = {
        assignTo: this.props.assignTo,
        status: this.props.status, //post status (pending, resolved or rejected)
        modal: false,
        comments: [],
        newComment: ''
    }
    componentWillMount() {
        const postId = this.props._id;
        axios.get(`/post/comment/${postId}`, { // get all the comments related to this post
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ comments: response.data.comments })
        }).catch((e) => {
            console.log('Error! Unable to get comments');
        })
    }

    toggle = () => {
        this.setState({
            modal: !this.state.modal
        });
    }

    onUserTypeChange = (e) => {
        const assignTo = e.target.value;
        const data = {
            postId: this.props._id,
            assignTo
        }
        axios.post('/post/assignto', data, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then(() => {
            this.setState({ assignTo })
        }).catch((e) => {
            alert('unable to assign')
            console.log(e);
        })
    }
    onPostStatusChange = (e) => {
        const status = e.target.value;
        const data = {
            postId: this.props._id,
            status
        }
        axios.post('/post/status', data, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then(() => {
            this.setState({ status })
        }).catch((e) => {
            alert('unable to change the status')
            console.log(e);
        })
    }
    handlenewCommentChange = (e) => {
        this.setState({ newComment: e.target.value })
    }
    //new comment post handler
    postComment = (e) => { 
        e.preventDefault();
        const text = e.target.newComment.value;
        if(!text) {
            return;
        }
        axios.post(`/post/comment/${this.props._id}`, { text }, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState( (prevState) => ({
                comments: [...prevState.comments, response.data.comment],
                newComment: ''
            }))
        }).catch((e) => {
            console.log(e);
            alert('Unable to post comment');
        })
    }
    render() {
        return (
            <tr>
                <td data-label="subject" onClick={this.toggle} style={{ color: 'blue', cursor: 'pointer' }}>{this.props.subject}</td>
                <td data-label="coords">{this.props.latitude}<br />{this.props.longitude}</td>
                <td data-label="created At">{moment(this.props.createdAt).format('llll')}</td>
                {this.props.currentUserType == 1 && (
                    <td data-label="assign To">
                        <Input type="select" value={this.state.assignTo || 'none'} onChange={this.onUserTypeChange}>
                            <option value="none">not assigned</option>
                            {
                                this.props.typeFourUsers.map((user) => <option key={user._id} value={user._id}>{user.fullName}</option>)
                            }
                        </Input>
                    </td>
                )}
                {this.props.currentUserType == 4 && (
                    <td data-label="post status">
                        <Input type="select" value={this.state.status} onChange={this.onPostStatusChange}>
                            <option value="pending">pending</option>
                            <option value="resolved">resolve</option>
                            <option value="rejected">rejected</option>
                        </Input>
                    </td>
                )}
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalBody>
                        <div className="modal-body__carousel">
                            <Button 
                                color="primary" 
                                size="sm" 
                                onClick={this.toggle} 
                                className="carousel-close show-for-mobile"
                            >
                                Close
                            </Button>
                            <UncontrolledCarousel items={
                                this.props.images.map((image) => {
                                    return { src: `/images/${image}`, caption: '' }
                                })} 
                            />
                        </div>
                        <div className="modal-body__post-info">
                            <div className="post-info">
                                <img src={`/images/${this.props.images[0]}`} alt="avatar" className="avatar"/>
                                <p>
                                    <strong>Vivek mishra</strong><br/>
                                    {moment(this.props.createdAt).format('llll')}
                                </p>
                            </div>
                            <div className="comments">
                                <ul className="comments__main">
                                    {this.state.comments.map((comment, index) => <Comment comment={comment} key={index+1}/>)}
                                </ul>
                                <form onSubmit={this.postComment} className="comments-form">
                                    <Input 
                                        type="text" 
                                        name="newComment" 
                                        placeholder="Write a comment here..."
                                        value={this.state.newComment}
                                        onChange={this.handlenewCommentChange}
                                    >
                                    </Input>
                                    <Button>post</Button>
                                </form>
                            </div>
                        </div>
                    </ModalBody>
                </Modal>
            </tr>
        )
    }
}

export default PostListItem;