import React from 'react';
import { connect } from 'react-redux';
import { Button, Jumbotron } from 'reactstrap';
import MdSend from 'react-icons/lib/md/send';
import axios from 'axios';
import PlacesSearchBox from './maps/PlacesSearchBox';
import MapWithADirectionsRenderer from './maps/MapWithADirectionsRenderer';
import { setOriginLocation, setDestinationLocation } from '../actions/ambulance';
import { socket } from '../actions/socket';
import { ListGroup, ListGroupItem, Badge} from 'reactstrap';
import AmbulancePageUid3 from './AmbulancePageUid3';

class AmbulancePage extends React.Component {
    submitOriginLocation = (location) => {
        this.props.dispatch(setOriginLocation(location));
    }
    submitDestinationLocation = (location) => {
        this.props.dispatch(setDestinationLocation(location));
    }
    searchAmbulance = () => {
        const originLocation = this.props.originLocation;  // obj with place_id, lat, lng, formatted_address
        if(!originLocation) { return }
        socket.emit('search ambulance', originLocation, (error) => {
            alert(error);
        })
    }
    bookAmbulance = () => {
        socket.emit('book ambulance', {
            originLocation: this.props.originLocation,
            destinationLocation: this.props.destinationLocation
        })
    }
    cancelAmbulace = () => {
        if(confirm('Do you want to cancel the ambulance ?')) {
            socket.emit('cancel ambulance', {
                uid: this.props.bookedAmbulanceInfo.ambulanceUser_uid  //ambulace user id to cancel the available status
            });
        }
    }
    render() {
        if(this.props.userType == 3) {
            return <AmbulancePageUid3 
                bookedAmbulanceInfo={this.props.bookedAmbulanceInfo}
            />
        }
        return (
          <div className="dashboard-main">
          {
            !this.props.bookedAmbulanceInfo && 
                <Jumbotron>
                    <h1 className="display-3">Call Ambulance - We Save Lives. Faster</h1>
                    <p className="lead"></p>
                    <hr className="my-2" />
                </Jumbotron>
          }
                <div className="booking-container">
                    {
                        !this.props.bookedAmbulanceInfo && (
                            <div className="booking">
                                <PlacesSearchBox submitLocation={this.submitOriginLocation} placeholder="Enter pickup location" />
                                <div className="booking-destination">
                                    <PlacesSearchBox submitLocation={this.submitDestinationLocation} placeholder="Enter destination location" />
                                    <div
                                        disabled={!this.props.originLocation || !this.props.destinationLocation}
                                        onClick={this.searchAmbulance}
                                    >
                                        <MdSend />
                                    </div>
                                </div>
                            </div>
                        )
                    }
                    {
                        (!this.props.bookedAmbulanceInfo && this.props.availableAmbulancesInfo.length) && (
                            <ListGroup className="booking-info">
                                <ListGroupItem 
                                        className="justify-content-between"
                                    >
                                        Total available ambulances <Badge pill>{this.props.availableAmbulancesInfo.length}</Badge>
                                        <Button onClick={this.bookAmbulance} color="warning" size="sm" style={{float: 'right'}}>Book now</Button>
                                </ListGroupItem>
                                {this.props.availableAmbulancesInfo.map((ambulance, index) => 
                                    <ListGroupItem 
                                        className="justify-content-between"
                                        key={index+1}
                                    >
                                        Currently at {ambulance.distance} from you, will reach to you in <Badge pill>{ambulance.duration}</Badge>
                                    </ListGroupItem>
                                )}
                            </ListGroup>
                        )
                    }
                </div>
                {
                    this.props.bookedAmbulanceInfo && (
                        <div>
                            <Jumbotron>
                                <h1 className="display-3">Ambulance Booked!</h1>
                                <p className="lead">Your ambulance is heading towards you.</p>
                                <hr className="my-2" />
                                <p>Your Ambulace driver is {this.props.bookedAmbulanceInfo.ambulanceUser_fullName}, mobile - {this.props.bookedAmbulanceInfo.ambulanceUser_mobile}</p>
                                <p className="lead">
                                <Button color="danger" onClick={this.cancelAmbulace}>Cancel</Button>
                                </p>
                            </Jumbotron>
                        </div>
                    )
                }
            {this.props.bookedAmbulanceInfo && this.props.bookedAmbulanceInfo.currentLocation &&
                <MapWithADirectionsRenderer 
                    origin={this.props.bookedAmbulanceInfo ? this.props.bookedAmbulanceInfo.origin : {lat: 0, lng: 0}}
                    destination={this.props.bookedAmbulanceInfo ? this.props.bookedAmbulanceInfo.currentLocation : {lat: 0, lng: 0}}
                />
            }
          </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userType: state.auth.userType,
    originLocation: state.ambulance.originLocation,
    destinationLocation: state.ambulance.destinationLocation,
    bookedAmbulanceInfo: state.ambulance.bookedAmbulanceInfo,
    availableAmbulancesInfo: state.ambulance.availableAmbulancesInfo || []
})

export default connect(mapStateToProps)(AmbulancePage);

