import React from 'react';
import { Table, Collapse, Button, CardBody, Card, Form, Input } from 'reactstrap';
import UserTypeListItem from './UserTypeListItem';

class UserTypeList extends React.Component {
    createUserType = (e) => {
        e.preventDefault();
        const displayName = e.target.displayName.value;
        const description = e.target.description.value;
        this.props.createUserType(displayName, description);
        e.target.displayName.value = '';
        e.target.description.value = '';
    }  
    render() {
        return (
            <div>
                <Form onSubmit={this.createUserType} className="userTypeCreate-form">
                    <Input type="text" name="displayName" placeholder="Name of user type" required/>
                    <Input type="text" name="description" placeholder="Description" required/>
                    <div><Button color="warning">Create new user type</Button></div>
                </Form>
                <Table bordered striped>
                    <thead>
                        <tr>
                            <th scope="col">Index</th>
                            <th scope="col">User types</th>
                            <th scope="col">Description</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.userTypes.map((userType, index) =>
                                <UserTypeListItem 
                                    {...userType} 
                                    itemNo={index+1} 
                                    key={userType._id}
                                    deleteUserType={this.props.deleteUserType}
                                />
                            )
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default UserTypeList;

