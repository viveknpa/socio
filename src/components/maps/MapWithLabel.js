import React from 'react';
const { compose, withProps } = require("recompose");
const {
  withGoogleMap,
  GoogleMap,
} = require("react-google-maps");
const { MarkerWithLabel } = require("react-google-maps/lib/components/addons/MarkerWithLabel");

const MapWithLabel = compose(
	withProps({
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={17}
    defaultCenter={props.location}
  >
    <MarkerWithLabel
      position={props.location}
      labelAnchor={new google.maps.Point(0, 0)}
      labelStyle={{backgroundColor: "yellow", fontSize: "32px", padding: "16px"}}
    >
      <div>{props.label}</div>
    </MarkerWithLabel>
  </GoogleMap>
);

export default MapWithLabel;