import React from 'react';
import { Input } from 'reactstrap';
const { compose, withProps, lifecycle } = require("recompose");
const { StandaloneSearchBox } = require("react-google-maps/lib/components/places/StandaloneSearchBox");

const PlacesSearchBox = compose(
  lifecycle({
    componentWillMount() {
      const refs = {}

      this.setState({
        places: [],
        onSearchBoxMounted: ref => {
          refs.searchBox = ref;
        },
        onPlacesChanged: () => {
          const places = refs.searchBox.getPlaces();
          const { place_id, formatted_address, geometry: {location: {lat, lng}}  } = places[0];
          this.props.submitLocation({ place_id, formatted_address, latitude: lat(), longitude: lng() });
          this.setState({
            places,
          });
        },
      })
    },
  }) 
)(props =>
  <div data-standalone-searchbox="">
   
    <StandaloneSearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      onPlacesChanged={props.onPlacesChanged}
    >
      <Input
        type="text"
        placeholder={props.placeholder}
      />
    </StandaloneSearchBox>
  </div>
);

export default PlacesSearchBox;