import React from 'react';
import {
    Jumbotron,
    Container,
    Button
} from 'reactstrap';
import axios from 'axios';
import Select from 'react-select';

class SettingPage extends React.Component {
    state = {
        headLocality: undefined,  //previously added {state, city}
        states: [],
        selectedState: '',
        districts: [],
        selectedDistrict: ''
    }
    config = { headers: {'x-auth': localStorage.getItem('x-auth')} };

    handleStateChange = (selectedOption) => {
        if(!selectedOption) {
            return this.setState({ selectedState: '', districts: [] });
        }
        const selectedState = selectedOption.value;
        this.setState({ selectedState });
        axios.post('/districtlist', {state: selectedState}, this.config).then((response) => {
            this.setState({ districts: response.data.districts });
        }).catch((e) => {
            console.log(e);
        })
    }
    handleDistrictChange = (selectedOption) => {
        if(!selectedOption) {
            this.setState({ selectedDistrict: '' });
            return;
        }
        this.setState({ selectedDistrict: selectedOption.value });
    }
    addHeadLocality = () => {
        const state = this.state.selectedState;
        const district = this.state.selectedDistrict;
        if(!state || !district) { return };
        axios.post('/headlocality', {state, district}, this.config).then((response) => {
            this.setState({ headLocality: response.data.headLocality })
        }).catch((e) => {
            alert('Unable to add locality');
        })
    }
    componentWillMount() {
        axios.get('/statelist', this.config).then((response) => {
            this.setState({ states: response.data.states })
        }).catch((e) => {
            console.log(e);
        })
        axios.get('/headlocality', this.config).then((response) => {
            this.setState({ headLocality: response.data.headLocality })
        }).catch((e) => {
            console.log(e);
        })
    }
    render() {
        return (
            <div className="dashboard-main">
                <Jumbotron>
                    <h1 className="display-3">Settings</h1>
                    <hr className="my-2" />
                </Jumbotron>
                <Container>
                    {this.state.headLocality && 
                        <p style={{fontWeight: 'bold', color: 'green'}}><br/>Currently active State - {this.state.headLocality.state}, 
                        City - {this.state.headLocality.district}<br />
                        </p> 
                    }
                    <h3>Select State</h3>
                    <Select
                        name="state"
                        clasName="selectState"
                        value={this.state.selectedState}
                        onChange={this.handleStateChange}
                        options={this.state.states.map((state) => {
                            return { value: state, label: state };
                        })}
                    /><br />
                    <h3>Select District</h3>
                    <Select
                        name="district"
                        clasName="selectDistrict"
                        value={this.state.selectedDistrict}
                        onChange={this.handleDistrictChange}
                        options={this.state.districts.map((district) => {
                            return { value: district, label: district };
                        })}
                    />
                    <br />
                    <Button 
                        color="info" 
                        onClick={this.addHeadLocality}
                    >
                        Save
                    </Button>
                </Container>
            </div>
        )
    }
}

export default SettingPage;