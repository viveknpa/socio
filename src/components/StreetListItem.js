import React from 'react';

class StreetListItem extends React.Component {
    render() {
        return (
            <tr>
                <td data-label="Note">{this.props.note}</td>
                <td data-label="Locality">{this.props.locality}</td> 
                <td data-label="Location">({this.props.location.lat}, {this.props.location.lng})</td> 
                <td data-label="Street number">{this.props.streetNumber}</td>
            </tr>
        )
    }
}

export default StreetListItem;