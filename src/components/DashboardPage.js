import React from 'react';
import Users from './Users';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

export const DashboardPage = (props) => (
    props.userType === 1 || props.userType === 4 ? (
        <div className="dashboard-main">
            <Users />
        </div>
    ) : (
        <Redirect to="/dashboard/viewPost" />
    )
);

const mapStateToProps = (state) => ({
    userType: state.auth.userType
})

export default connect(mapStateToProps)(DashboardPage);