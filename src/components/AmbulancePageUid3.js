import React from 'react';
import { Button, Alert } from 'reactstrap';

const AmbulancePageUid3 = (props) => (
    <div className="dashboard-main">
        <Alert color="success">
            {
                !props.bookedAmbulanceInfo ? (
                    <div>
                        <h4 className="alert-heading">No activity!</h4>
                    </div>
                ) : (
                    <div>
                        <h4 className="alert-heading">Booked!</h4>
                        <p>
                            Contact your patient at Mobile - {props.bookedAmbulanceInfo.ambulanceBooker_mobile}, Name - {props.bookedAmbulanceInfo.ambulanceBooker_fullName}
                        </p>
                        <hr />
                        { props.bookedAmbulanceInfo.currentLocation && 
                            <div>
                                <p className="mb-0">
                                    <a target="_blank" 
                                        href={`https://www.google.com/maps/dir/?api=1&origin=${props.bookedAmbulanceInfo.currentLocation.lat},${props.bookedAmbulanceInfo.currentLocation.lng}&destination=${props.bookedAmbulanceInfo.origin.lat},${props.bookedAmbulanceInfo.origin.lng}&travelmode=driving`}
                                    >
                                        Click here to get the directions to the patient
                                    </a>
                                </p>
                                <p className="mb-0">
                                    <a target="_blank" 
                                        href={`https://www.google.com/maps/dir/?api=1&origin=${props.bookedAmbulanceInfo.currentLocation.lat},${props.bookedAmbulanceInfo.currentLocation.lng}&destination=${props.bookedAmbulanceInfo.destination.lat},${props.bookedAmbulanceInfo.destination.lng}&travelmode=driving`}
                                    >
                                        Click here to get the directions from patient
                                    </a>
                                </p>
                            </div>
                        }
                    </div>
                )
            }
        </Alert>
    </div>
)

export default AmbulancePageUid3;