import React from 'react';
import classnames from 'classnames';
import axios from 'axios';
import {
    Button,
    Jumbotron,
    TabContent, TabPane, Nav, NavItem, NavLink, Card, CardTitle, CardText, Row, Col
} from 'reactstrap';
import CleanlinessComplaint from './CleanlinessComplaint';
import StreetComplaint from './StreetComplaint';

class AddComplaintPage extends React.Component {
    state = {
        activeTab: '1'
    }
    toggle = (tab) => {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
    }
    render() {
        return (
            <div className="dashboard-main">
                <Jumbotron>
                    <h1 className="display-3">Add Complaints</h1>
                    <hr className="my-2" />
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '1' })}
                                onClick={() => { this.toggle('1'); }}
                            >
                                Cleanliness
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '2' })}
                                onClick={() => { this.toggle('2'); }}
                            >
                                Street
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Jumbotron>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <CleanlinessComplaint />
                    </TabPane>
                    <TabPane tabId="2">
                        <StreetComplaint />
                    </TabPane>
                </TabContent>
            </div>
        )
    }
}

export default AddComplaintPage;