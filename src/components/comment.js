import React from 'react';
import axios from 'axios';
import { Input, Button} from 'reactstrap';
import moment from 'moment';

class Comment extends React.Component {
    state = {
        reply: this.props.comment.reply,
        replyText: ''
    }
    handleReplyTextChange = (e) => {
        this.setState({ replyText: e.target.value })
    }
    handleReplySubmit = (e) => {
        const text = e.target.value.trim();
        if(!text) { return }
        if (e.keyCode === 13) {
            axios.post(`/post/comment/reply/${this.props.comment._id}`, { text }, {
                headers: { 'x-auth': localStorage.getItem('x-auth') }
            }).then((response) => {
                this.setState((prevState) => ({
                    reply: [...prevState.reply, response.data.reply],
                    replyText: ''
                }))
            }).catch((e) => {
                alert('Unable to post reply');
                console.log(e);
            })
        }
    }
    render() {
        return (
            <li className="comment">
                <span className="comment__main">
                    <strong>
                        {this.props.comment.commentedBy_fullName}
                    </strong>&ensp;
                    {this.props.comment.text}
                </span>
                <div className="reply">
                    {
                        this.state.reply.map((replyItem, index) => {
                            return (
                                <span key={index+1} className="reply__main">
                                    <strong>
                                        {replyItem.repliedBy_fullName}
                                    </strong>
                                    {replyItem.text}
                                    <br/>
                                </span>
                            )
                        })
                    }
                    <Input 
                        type="text" 
                        placeholder="write a reply" 
                        value={this.state.replyText}
                        onChange={this.handleReplyTextChange}
                        onKeyDown={this.handleReplySubmit}
                    >
                    </Input>
                </div>
            </li>
        )
    }
}

export default Comment;