import React from 'react';
import classnames from 'classnames';
import axios from 'axios';
import moment from 'moment';
import {
    Button,
    Jumbotron,
    Table,
    TabContent, TabPane, Nav, NavItem, NavLink, Card, CardTitle, CardText, Row, Col
} from 'reactstrap';
import { connect } from 'react-redux';
import AddStreet from './AddStreet';
import MapWithAMarkerClusterer from './maps/MapWithAMarkerClusterer';
import StreetList from './StreetList';

class StreetViewPage extends React.Component {
    state = {
        streets: [],
        complaints: [],
        activeTab: '1'
    }
    toggle = (tab) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    addStreet = (street) => {
        this.setState((prevState) => ({
            streets: [...prevState.streets, street]
        }))
    }
    componentWillMount() {
        axios.get('/streets', {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ streets: response.data.streets })
        }).catch((e) => {
            console.log(e);
        })
        axios.get('/post/street', {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ complaints: response.data.complaints })
        })
    }
    render() {
        return (
            <div className="dashboard-main">
                <Jumbotron>
                    <h1 className="display-3">Street View</h1>
                    <hr className="my-2" />
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '1' })}
                                onClick={() => { this.toggle('1'); }}
                            >
                                Map view
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '2' })}
                                onClick={() => { this.toggle('2'); }}
                            >
                                Add street
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '3' })}
                                onClick={() => { this.toggle('3'); }}
                            >
                                Modify street
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '4' })}
                                onClick={() => { this.toggle('4'); }}
                            >
                                Complaints
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Jumbotron>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <MapWithAMarkerClusterer markers={this.state.streets} />
                    </TabPane>
                    <TabPane tabId="2">
                        <AddStreet addStreet={this.addStreet} streets={this.state.streets} tabChange={this.toggle} />
                    </TabPane>
                    <TabPane tabId="3">
                        <StreetList streets={this.state.streets} />
                    </TabPane>
                    <TabPane tabId="4">
                        {
                            this.state.complaints.length ? (
                                <Table bordered striped>
                                    <thead>
                                        <tr>
                                            <th scope="col">Index</th>
                                            <th scope="col">Complaint Date</th>
                                            <th scope="col">Complaint by</th>
                                            <th scope="col">Subject</th>
                                            <th scope="col">Street No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.complaints.map((complaint, index) => (
                                                <tr key={index+1}>    
                                                    <td data-label="Index">{ index + 1 }</td>
                                                    <td data-label="date">{ moment(complaint.createdAt).format("MMM Do YY")  }</td>
                                                    <td data-label="by">{ complaint.creatorName }</td> 
                                                    <td data-label="subject">{ complaint.subject }</td> 
                                                    <td data-label="Street no">{ complaint.streetNumber }</td> 
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </Table>
                            ) : (
                                    <div
                                        style={{
                                            height: '100px',
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            border: '1px solid #ccc',
                                            fontSize: '23px',
                                            color: 'green'
                                        }}
                                    >
                                        No Complaint added!
                                </div>
                                )}
                    </TabPane>
                </TabContent>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userType: state.auth.userType
})

export default connect(mapStateToProps)(StreetViewPage);