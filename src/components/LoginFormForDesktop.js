import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Input, FormText } from "reactstrap";

export default class LoginForm extends React.Component {
    onLoginSubmit = (e) => {
        e.preventDefault();
        this.props.startLogin({
            email: e.target.email.value,
            password: e.target.password.value
        });
    }
    render() {
        return (
            <Form className="nav-form" style={{ display: 'flex', alignItems: 'center' }} onSubmit={this.onLoginSubmit}>
                <FormGroup style={{ marginRight: '10px', marginBottom: 0 }}>
                    <Input
                        style={{ height: '35px' }}
                        type="email"
                        name="email"
                        placeholder="Email"
                    />
                </FormGroup>
                <FormGroup style={{ marginRight: '10px', marginBottom: 0 }}>
                    <Input
                        style={{ height: '35px' }}
                        type="password"
                        name="password"
                        placeholder="Password"
                    />
                </FormGroup>
                <Button outline style={{ height: '32px', padding: '0 20px', borderColor: 'white', marginRight: '20px' }}>Login</Button>
                <Link to="/forgot" style={{ color: 'white', fontSize: '12px' }}>forgot password ?</Link>
            </Form>
        )
    }
}