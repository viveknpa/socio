import React from 'react';
import UserListItem from './UserListItem';
import { Table } from 'reactstrap';
import Select from 'react-select';

class UserList extends React.Component {
    state = {
        users: [],
        filterText: '',
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ users: nextProps.users })
    }
    handleFilterTextChange = (selectedOption) => {
        if(selectedOption) {
            const filterText = selectedOption.value;
            this.setState({ filterText });
            this.setState({ users: this.props.users.filter((user) => user.userType == filterText) })
        } else {
            this.setState({ users: this.props.users, filterText: '' })
        }
    }
    render() {
        if(!this.props.users.length) {
            return (
                <div style={{textAlign: 'center', marginTop: '50px'}}>
                   <img className="loader__image" src="/images/loader.gif"/>
                </div>
            )
        }
        return (
            <div>
                <div className="usersFilterContainer">
                    <span>Filter by user type: </span>
                    <Select
                        name="locality"
                        clasName="selectUserType"
                        value={this.state.filterText}
                        onChange={this.handleFilterTextChange}
                        options={this.props.userTypes.map((userType) => {
                            const { type, displayName } = userType;
                            return { value: type, label: displayName };
                        })}
                    />
                </div>
                <Table bordered striped>
                    <thead>
                        <tr>
                            <th scope="col">Index</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Mobile</th>
                            <th scope="col">User Type</th>
                            <th scope="col">active</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.users.map((user, index) =>
                                <UserListItem
                                    key={user._id}
                                    index={index + 1}
                                    {...user}
                                    userTypes={this.props.userTypes}
                                    currentUserType={this.props.currentUserType}
                                    handleUserUpdate={this.props.handleUserUpdate}
                                />
                            )
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default UserList;