import React from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem
} from "reactstrap";
import { Container, Button, Form, FormGroup, Alert, Input, FormText } from "reactstrap";

export default class ResetPassword extends React.Component {
    state = {
        resetStatus: false,
        error: ''
    }
    onSubmit = (e) => {
        e.preventDefault();
        const password = e.target.password.value;
        const confirmPassword = e.target.confirmPassword.value;
        if(!password) {
            this.setState({ error: 'Password cannot be empty' });
        } else if(!confirmPassword) {
            this.setState({ error: 'confirm your password' });
        } else if(password !== confirmPassword) {
            this.setState({ error: 'Passwords do not match' })  
        } else {
            const config = {};
            let url;
            const token = localStorage.getItem('x-auth');
            if(token) {
                config.headers = { 'x-auth': token };
                url = '/reset';
                console.log(url, config);
            } else {
                url = `/reset/${this.props.match.params.secretToken}`;
            }
            return axios.post(url, { password }, config).then(() => {
                this.setState({error: ''});
                this.setState({resetStatus: true});
            }).catch((e) => {
                this.setState({error: 'Unable to reset password, try later'});
            })
        }
    }

    render() {
        return (
            <div>
                <Navbar light expand="md" style={{ background: '#0747A6' }}>
                    <Container>
                        <NavbarBrand tag={Link} to="/" style={{ color: 'white' }}>Logo</NavbarBrand>
                    </Container>
                </Navbar>
                <Container>
                <div style={{ display: 'flex', justifyContent: 'center', marginTop: '50px' }}>
                        <Form style={{ maxWidth: '400px' }} onSubmit={this.onSubmit}>
                            <h1 style={{fontWeight: 200}}>Change your password</h1>
                            {this.state.resetStatus && <Alert color="success">Password reset successful <Link to="/"> Go back</Link></Alert>}
                            {this.state.error && <Alert color="danger">{this.state.error}</Alert>}  
                            <FormGroup>
                                <Input
                                    type="password"
                                    name="password"
                                    placeholder="Password(min 6 characters)" 
                                />
                            </FormGroup>
                            <FormGroup>
                                <Input
                                    type="password"
                                    name="confirmPassword"
                                    placeholder="confirm password" 
                                />
                            </FormGroup>
                            <Button >Change password</Button>
                        </Form>
                    </div>
                    </Container>
            </div>
        )
    }
}