import React from 'react';
import axios from 'axios';
import { Table, Input, Button, Jumbotron} from 'reactstrap';
import { connect } from 'react-redux';
import ReportListItem from './ReportListItem';

class ReportListPage extends React.Component {
    render() {
        return (
            <div className="dashboard-main">
                <Jumbotron>
                    <h1 className="display-3">Posts Report</h1>
                    <p className="lead"></p>
                    <hr className="my-2" />
                </Jumbotron>
                <Table bordered striped>
                    <thead>
                        <tr>
                            {this.props.userType === 4 ? (
                                <th scope="col">Name</th>
                            ) : (
                                    <th scope="col">Name of type four users</th>
                                )}
                            <th scope="col">Total post assigned</th>
                            <th scope="col">Posts pending</th>
                            <th scope="col">Posts resolved</th>
                            <th scope="col">Posts rejected</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.report.map((element) =>
                                <ReportListItem key={element._id} {...element} currentUserType={this.props.userType} />
                            )
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userType: state.auth.userType,
    report: state.report
})

export default connect(mapStateToProps)(ReportListPage);