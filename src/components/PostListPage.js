import React from 'react';
import axios from 'axios';
import PostListItem from './PostListItem';
import { history } from '../routers/AppRouter';
import { Table, Input, Button, Jumbotron} from 'reactstrap';
import FaSearch from 'react-icons/lib/fa/search';
import { connect } from 'react-redux';

class PostListPage extends React.Component {
    state = {
        posts: [],
        typeFourUsers: [],
        reportOf: '',
        loader: true
    }
    componentWillMount() {
        const config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }
        const status = this.props.match.params.status;
        const typeFourUid = this.props.match.params.typeFourUid;
        if (status && typeFourUid) {
            const posts = this.props.report.filter((reportItem) => reportItem._id === typeFourUid)[0];
            this.setState({ posts: posts[`${status}Posts`], reportOf: posts.fullName });
        } else {
            axios('/post', config).then((response) => {  //get all posts
                this.setState({ posts: response.data.posts, loader: false });
            }).catch((e) => {
                console.log(e);
            })
        }
        if (this.props.userType == 1) {
            axios.get('/users/typeFour', config).then((response) => {
                this.setState({ typeFourUsers: response.data.users })
            })
        }
    }
    render() {
        if(this.state.loader) {
            return (
                <div style={{textAlign: 'center', marginTop: '80px'}} className="dashboard-main">
                   <img className="loader__image" src="/images/loader.gif"/>
                </div>
            )
        }
        return (
            <div className="dashboard-main">
                <Jumbotron>
                    {this.state.reportOf ? (
                        <h1 className="display-3">{this.props.match.params.status} post's Report of {this.state.reportOf}</h1>)
                        : (
                            <h1 className="display-3">Recent posts</h1>
                        )}
                    <hr className="my-2" />
                </Jumbotron>
                {
                    this.state.posts.length ? (
                        <Table bordered striped>
                            <thead>
                                <tr>
                                    <th scope="col">Subject</th>
                                    <th scope="col">(Lat, Lng)</th>
                                    <th scope="col">Created At</th>
                                    {this.props.userType === 1 && <th scope="col">Assign To</th>}
                                    {this.props.userType === 4 && <th scope="col">Change status</th>}
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.posts.map((post) =>
                                        <PostListItem key={post._id} {...post} typeFourUsers={this.state.typeFourUsers} currentUserType={this.props.userType} />
                                    )
                                }
                            </tbody>
                        </Table>
                    ) : (
                        <div
                            style={{
                                height: '100px',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                                border: '1px solid #ccc',
                                fontSize: '23px',
                                color: 'green'   
                            }}
                        >
                            No Complaint added yet!
                        </div>
                    )
                }
            </div>
        )
    }
}

const mapDispatchToProps = (state) => ({
    userType: state.auth.userType,
    report: state.report
})

export default connect(mapDispatchToProps)(PostListPage);