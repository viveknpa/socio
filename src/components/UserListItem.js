import React from 'react';
import axios from 'axios';
import { Input, Button } from 'reactstrap';

class UserListItem extends React.Component {
    state = {
        _id: this.props._id,
        email: this.props.email,
        userType: this.props.userType,
        active: this.props.active,
        displaySaveButton: false,
    }
    onActiveChange = (e) => {
        this.setState((prevState) => ({
            active: !prevState.active,
            displaySaveButton: true,
        }))
    }
    onUserTypeChange = (e) => {
        this.setState({ userType: e.target.value, displaySaveButton: true })
    }
    handleUserUpdate = () => {
        const {_id, userType, active} = this.state;
        this.props.handleUserUpdate(_id, userType, active).then(() => {
            this.setState({displaySaveButton: false})
        }).catch((e) => {
            alert('Unable to update user');
            console.log(e);
        })
        // axios({
        //     method: 'post',
        //     url: `/users/userupdate`,
        //     data,
        //     headers: {'x-auth': localStorage.getItem('x-auth')}
        // }).then((response) => {
        //     this.setState({displaySaveButton: false})
        // }).catch((e) => {
        //     console.log(e);
        // })
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ userType: nextProps.userType })
    }
    render() {
        return (
            <tr>
                <td data-label="Index">{this.props.index}</td>
                <td data-label="name">{this.props.fullName}</td>
                <td data-label="email">{this.props.email}</td>
                <td data-label="mobile">{this.props.mobile}</td>
                {
                    this.props.currentUserType === 1 ? (
                        <td data-label="user type">
                            <Input type="select" value={this.state.userType} onChange={this.onUserTypeChange}>
                                {
                                   this.props.userTypes.map((element) => 
                                        <option key={element._id} value={element.type}>{element.displayName}</option>
                                )}
                            </Input>
                        </td>
                    ) :  (
                        <td data-label="user type">{this.props.userType}</td>
                    )
                }
                {
                    this.props.currentUserType === 1 ? (
                        <td data-label="account active">
                            <input type="checkbox" checked={this.state.active} onChange={this.onActiveChange} />
                            {this.state.displaySaveButton && (
                                <Button 
                                    color="link"
                                    style={{float: 'right', padding: 0}}
                                    onClick={this.handleUserUpdate}
                                >
                                    Save
                                </Button>
                            )}
                        </td>
                    ) : (
                        <td data-label="account active">{this.state.active ? 'Yes' : 'No'}</td>
                    )
                }   
            </tr>
        )
    }
}

export default UserListItem;