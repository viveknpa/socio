import React from 'react';
import { connect } from 'react-redux';
import { startSignUp, startLogin } from '../actions/auth';
import LoginFormForMobile from './LoginFormForMobile';
import LoginFormForDesktop from './LoginFormForDesktop';
import SignupForm from './SignupForm';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from "reactstrap";
import { Container, Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";

export class LoginPage extends React.Component {
    state = {
        showSignupForm: true,
        isOpen: false
    }
    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    handleFormDisplay = () => {
        this.setState((prevState) => ({
            showSignupForm: !prevState.showSignupForm
        }))
    }
    handleForm
    render() {
        return (
            <div>
                <Navbar light expand="md" style={{ background: '#0747A6' }}>
                    <Container>
                        <NavbarBrand style={{ color: 'white' }}>Logo</NavbarBrand>
                        <NavbarToggler onClick={this.toggle} className="show-for-desktop" />
                        {/* need to remove className="show for desktop if some anchors availbale in dropdown for mobile"*/}
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar style={{ display: 'flex', alignItems: 'center' }}>
                                <NavItem>
                                    <div className="show-for-desktop">
                                        {this.props.loginError && <span style={{ color: '#D04250' }}>{this.props.loginError}</span>}
                                        <LoginFormForDesktop
                                            startLogin={this.props.startLogin}
                                        />
                                    </div>
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </Container>
                </Navbar>
                {this.state.showSignupForm ? (
                    <Container style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '50px' }}>
                        <SignupForm
                            startSignUp={this.props.startSignUp}
                            signUpError={this.props.signUpError}
                            handleFormDisplay={this.handleFormDisplay}
                        />
                    </Container>

                ) : (
                        <Container style={{ display: 'flex', justifyContent: 'center', marginTop: '50px' }}>
                            <LoginFormForMobile
                                startLogin={this.props.startLogin}
                                loginError={this.props.loginError}
                                handleFormDisplay={this.handleFormDisplay}
                            />
                        </Container>
                    )}

            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    loginError: state.auth.error.loginError,
    signUpError: state.auth.error.signUpError
})

const mapDispatchToProps = (dispatch) => ({
    startSignUp: (user) => dispatch(startSignUp(user)),
    startLogin: (user) => dispatch(startLogin(user))
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);