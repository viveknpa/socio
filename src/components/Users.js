import React from 'react';
import classnames from 'classnames';
import axios from 'axios';
import {
    Button,
    Jumbotron,
    TabContent, TabPane, Nav, NavItem, NavLink, Card, CardTitle, CardText, Row, Col
} from 'reactstrap';
import { connect } from 'react-redux';
import UserList from './UserList';
import UserTypeList from './UserTypeList';
import Authority from './Authority';

class Users extends React.Component {
    state = {
        users: [],  //array of user doc 
        userTypes: [],  //will be assigned to an array containing all user types
        activeTab: '1'
    }
    toggle = (tab) => {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
    }
    createUserType = (displayName, description) => {
        axios.post('/usertype/create', {displayName, description}, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState((prevState) => ({
                userTypes: [...prevState.userTypes, response.data.userType]
            }))
        }).catch((e) => {
            alert('Unable to create new user type');
            console.log(e);
        })
    }
    deleteUserType = (id, userType) => {
        axios.delete(`/usertype/remove/${id}`, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then(() => {
            this.setState((prevState) => ({
                userTypes: prevState.userTypes.filter((userType) => userType._id !== id ),
                users: prevState.users.map((user) => {
                    if(user.userType == userType) {
                        user.userType = 5;
                        return user;
                    }
                    return user;
                })
            }))
        }).catch((e) => {
            alert('Unable to remove user type');
            console.log(e);
        })
    }
    handleUserUpdate = (id, userType, active) => {
        const data = {id, userType, active};
        return axios({
            method: 'post',
            url: `/users/userupdate`,
            data,
            headers: {'x-auth': localStorage.getItem('x-auth')}
        }).then(() => {
            this.setState((prevState) => ({
                users: prevState.users.map((user) => {
                    if(user._id == id) {
                        user.userType = userType;
                        return user;
                    }
                    return user;
                })
            }))
        })
    }
    componentWillMount() {
        axios.get('/getUsersList', {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ users: response.data.users })
        }).catch((e) => {
            console.log(e);
        })

        axios.get('/usertype/list', {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ userTypes: response.data.userTypes })
        }).catch((e) => {
            console.log(e);
        })
    }
    render() {
        return (
            <div>
                <Jumbotron>
                    <h1 className="display-3">User List</h1>
                    <p className="lead">Manage all the users</p>
                    <hr className="my-2" />
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '1' })}
                                onClick={() => { this.toggle('1'); }}
                            >
                                Register User
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '2' })}
                                onClick={() => { this.toggle('2'); }}
                            >
                                user types
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '3' })}
                                onClick={() => { this.toggle('3'); }}
                            >
                                Authority
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Jumbotron>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <UserList 
                            users={this.state.users} 
                            userTypes={this.state.userTypes} 
                            currentUserType={this.props.userType}
                            handleUserUpdate={this.handleUserUpdate}
                        />
                    </TabPane>
                    <TabPane tabId="2">
                        <UserTypeList 
                            userTypes={this.state.userTypes} 
                            createUserType={this.createUserType}
                            deleteUserType={this.deleteUserType}
                        /> 
                    </TabPane>
                    <TabPane tabId="3">
                        <Authority userTypes={this.state.userTypes}/> 
                    </TabPane>
                </TabContent>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userType: state.auth.userType
})

export default connect(mapStateToProps)(Users);