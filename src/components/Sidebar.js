import React from 'react';
import { NavLink } from 'react-router-dom';
import {FaGroup} from 'react-icons/lib/fa';
import { Collapse, Button, CardBody, Card, Input, ListGroup, ListGroupItem } from 'reactstrap';
import {connect} from 'react-redux';

class Sidebar extends React.Component {
    render() {
        return (
            <div className="sidebar show-for-desktop">
                <ListGroup>
                    {(this.props.userType === 1 || this.props.userType === 4) && (
                        <ListGroupItem tag={NavLink} exact={true} to="/dashboard/users" action>Users</ListGroupItem>
                    )}
                    <ListGroupItem tag={NavLink} exact={true} to="/dashboard/viewPost" action>View Complaint</ListGroupItem>
                    <ListGroupItem tag={NavLink} exact={true} to="/dashboard/addPost" action>Add Complaints</ListGroupItem>
                    {(this.props.userType === 1 || this.props.userType === 4) && (
                        <ListGroupItem tag={NavLink} exact={true} to="/dashboard/report" action>View report</ListGroupItem>
                    )}
                    {(this.props.userType === 1 || this.props.userType === 6) && (
                        <ListGroupItem tag={NavLink} exact={true} to="/dashboard/streetView" action>Street view</ListGroupItem>
                    )}
                    {(this.props.userType === 1 || this.props.userType === 3 || this.props.userType === 5) && (
                        <ListGroupItem tag={NavLink} exact={true} to="/dashboard/ambulance" action>Ambulance</ListGroupItem>
                    )}
                    {this.props.userType === 1 && (
                        <ListGroupItem tag={NavLink} exact={true} to="/dashboard/setting" action>Settings</ListGroupItem>
                    )}
                </ListGroup>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userType: state.auth.userType
})

export default connect(mapStateToProps)(Sidebar);