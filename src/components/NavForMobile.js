import React from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const NavForMobile = (props) => (
    <div className="show-for-mobile" style={{marginTop: '70px'}}>
        <Nav>
            <NavItem>
                {(props.userType === 1 || props.userType === 4) && (
                    <NavLink tag={Link} to="/dashboard/users">Users</NavLink>
                )}
            </NavItem>
            <NavLink tag={Link} to="/dashboard/viewPost">View Complaints</NavLink>
            <NavItem>
                <NavLink tag={Link} to="/dashboard/addPost" >Add Complaints</NavLink>
            </NavItem>
            <NavItem>
                {(props.userType === 1 || props.userType === 4) && (
                    <NavLink tag={Link} to="/dashboard/report">View report</NavLink>
                )}
            </NavItem>
            <NavItem>
                {(props.userType === 1 || props.userType === 6) && (
                    <NavLink tag={Link} to="/dashboard/streetView">Street view</NavLink>
                )}
            </NavItem>
			<NavItem>
                {(props.userType === 1 || props.userType === 3 || props.userType === 5) && (
                    <NavLink tag={Link} to="/dashboard/ambulance">Ambulance</NavLink>
                )}
            </NavItem>
            <NavItem>
                {(props.userType === 1) && (
                    <NavLink tag={Link} to="/dashboard/setting" >Settings</NavLink>
                )}
            </NavItem>
        </Nav>
        <hr />
    </div>
)

const mapStateToProps = (state) => ({
    userType: state.auth.userType
})

export default connect(mapStateToProps)(NavForMobile);