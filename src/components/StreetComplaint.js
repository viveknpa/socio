import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Container } from 'reactstrap';
import axios from 'axios';
import MapWithLabel from './maps/MapWithLabel';

class CleanlinessComplaint extends React.Component {
    state = {
        subject: '',
        streetNumber: '',
        location: undefined,
        message: ''
    }
    timer = undefined;
    matchStreet = () => {
        if(!navigator.geolocation) {
            return alert('geolocation not supported by your browser');
        }
        var geoOptions = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        }
        var geoError = (error) => {
            console.log('Error occurred. Error code: ' + error.code);
            // error.code can be:
            //   0: unknown error
            //   1: permission denied
            //   2: position unavailable (error response from location provider)
            //   3: timed out
        }; 
        navigator.geolocation.getCurrentPosition((position) => {
            const data = {};
            data.lat = position.coords.latitude;
            data.lng = position.coords.longitude;
            axios.post('/post/street/match', data, {
                headers: { 'x-auth': localStorage.getItem('x-auth') }
            }).then((response) => {
                this.setState({ 
                    streetNumber: response.data.streetNumber, 
                    location: response.data.location,
                    message: ''
                })
            }).catch((e) => {
                this.setState({ 
                    message: 'No street location match found'
                })
            })
        }, geoError, geoOptions)
    }
    onSubjectChange = (e) => {
        this.setState({ subject: e.target.value })
    }
    onSubmit = () => {
        const { subject, streetNumber } = this.state;
        const data = { subject, streetNumber };
        axios.post('/post/street', data, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then(() => {
            alert('Complaint added for street ' + this.state.streetNumber);
        }).catch((e) => {
            alert('unable to add complaint');
            console.log(e);
        })
    }
  render() {
    return (
        <Container>
            <Button outline color="info" type="button" onClick={this.matchStreet} style={{margin: '30px 0 5px 0'}}>Match with the street no</Button><br/>
            {this.state.message && <p style={{color: 'orange', margin: 0}}>{this.state.message}</p>}
            <br /><h3>Subject</h3>
            <Input type="text" name="subject" onChange={this.onSubjectChange}/><br />
            { this.state.location && <MapWithLabel location={this.state.location} label={this.state.streetNumber}/>}<br />
            <Button color="warning" onClick={this.onSubmit} disabled={!this.state.streetNumber}>Add Complaint</Button>
        </Container>
    )
  }
}

export default CleanlinessComplaint;