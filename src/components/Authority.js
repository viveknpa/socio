import React from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

class Authority extends React.Component {
    applyAuthority = (e) => {
        e.preventDefault();
        const authority = [];
        e.target.users.checked && authority.push('users');
        e.target.addComplaint.checked && authority.push('addComplaint');
        e.target.viewComplaint.checked && authority.push('viewComplaint');
        e.target.viewReport.checked && authority.push('viewReport');
        axios.patch(`/userType/edit/${e.target.userTypeId.value}`, { authority }, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            alert('User type authorised for the choosen fileds')
        }).catch((e) => {
            console.log(e);
        })
    }
    render() {
        return (
            <Form onSubmit={this.applyAuthority}>
                <FormGroup>
                    <Label for="userType">Select user type to choose authority</Label>
                    <Input type="select" name="userTypeId" >
                        {this.props.userTypes.map((element) => 
                            <option key={element._id} value={element._id}>{element.type}</option>
                        )}
                    </Input>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input type="checkbox" name="users" />
                        Manage Users
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input type="checkbox" name="addComplaint" />
                        Add Complaint
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input type="checkbox" name="viewComplaint" />
                        View complaint
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input type="checkbox" name="viewReport" />
                        View Report
                    </Label>
                </FormGroup>
                <Button>Submit</Button>
            </Form>
        )
    }
}

export default Authority;