import React from 'react';
import axios from 'axios';
import { Table, Input, Button } from 'reactstrap';
import StreetListItem from './StreetListItem';

class StreetList extends React.Component {
    render() {
        if(!this.props.streets.length) {
            return (
                <div
                    style={{
                        height: '100px',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        border: '1px solid #ccc',
                        fontSize: '23px',
                        color: 'green'   
                    }}
                >
                    No Street added!
                </div>
            )
        }

        return (
            <Table bordered striped>
                <thead>
                    <tr>
                        <th scope="col">Note</th>
                        <th scope="col">Locality</th>
                        <th scope="col">Location</th>
                        <th scope="col">Street number</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.streets.map((element, index) =>
                            <StreetListItem key={element._id} {...element} index={index + 1}/>
                        )
                    }
                </tbody>
            </Table>
        )
    }
}

export default StreetList;