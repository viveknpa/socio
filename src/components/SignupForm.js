import React from 'react';
import { Button, Form, FormGroup, Input, FormText, Alert } from "reactstrap";

export default class SignupForm extends React.Component {
    state = {
        validationError: ''
    }
    onSignupSubmit = (e) => {
        e.preventDefault();
        const fullName = e.target.fullName.value;
        const email = e.target.email.value;
        const mobile = e.target.mobile.value;
        const password = e.target.password.value;
        const confirmPassword = e.target.confirmPassword.value;
        if (!fullName) {
            this.setState({ validationError: 'full name is required' });
        } else if (!email) {
            this.setState({ validationError: 'email is required' });
        } else if (mobile.toString().length < 10) {
            this.setState({ validationError: 'Mobile no is invalid' });
        } else if (password.length < 6) {
            this.setState({ validationError: 'minimun 6 alphanumeric character is required for password' });
        } else if (password !== confirmPassword) {
            this.setState({ validationError: 'passwords do not match' });
        } else {
            this.setState({ validationError: '' });
            this.props.startSignUp({ fullName, email, mobile, password });
        }
    }
    render() {
        return (
            <Form onSubmit={this.onSignupSubmit}>
                <h1>Create a new account</h1>
                <p>It is free and always will be.</p>
                {this.state.validationError && <Alert color="danger"> {this.state.validationError} </Alert> }
                {this.props.signUpError && <Alert color="danger"> {this.props.signUpError} </Alert> }
                <FormGroup>
                    <Input
                        type="text"
                        name="fullName"
                        placeholder="full name"
                    />
                </FormGroup>
                <FormGroup>
                    <Input
                        type="email"
                        name="email"
                        placeholder="email" />
                </FormGroup>
                <FormGroup>
                    <Input
                        type="number"
                        name="mobile"
                        placeholder="Mobile number" />
                </FormGroup>
                <FormGroup>
                    <Input
                        type="password"
                        name="password"
                        placeholder="password" />
                </FormGroup>
                <FormGroup>
                    <Input
                        type="password"
                        name="confirmPassword"
                        placeholder="confirm password" />
                </FormGroup>
                <div>
                    <Button style={{ background: '#0747A6' }}>Join now</Button>
                </div>
                <span className="show-for-mobile">Already have an account?
                    <Button
                        onClick={this.props.handleFormDisplay}
                        color="link"
                        style={{paddingLeft: '0px', paddingTop: '0'}}
                    >
                    Log In
                    </Button>
                </span>
            </Form>
        )
    }
}
