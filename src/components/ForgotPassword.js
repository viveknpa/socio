import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem
} from "reactstrap";
import { Container, Button, Form, FormGroup, Alert, Input, FormText } from "reactstrap";

class ForgotPassword extends React.Component {
    state = {
        linkSent: false,
        error: ''
    }
    handleSubmit = (e) => {
        e.preventDefault();
        axios.post('/forgot', {
            email: e.target.email.value
        }).then(() => {
            this.setState({ error: '' });
            this.setState({ linkSent: true });
        }).catch((e) => {
            if (e.response.status == 404) {
                return this.setState({ error: 'Account not found' });
            }
            this.setState({ error: 'invalid email' });
        })
    }
    render() {
        return (
            <div>
                <Navbar light expand="md" style={{ background: '#0747A6' }}>
                    <Container>
                        <NavbarBrand tag={Link} to="/" style={{ color: 'white' }}>Logo</NavbarBrand>
                    </Container>
                </Navbar>
                <Container>
                    <div style={{ display: 'flex', justifyContent: 'center', marginTop: '50px' }}>
                        <Form style={{ maxWidth: '400px' }} onSubmit={this.handleSubmit}>
                            <h2 style={{fontWeight: '300'}}>Forgot your password?</h2>
                            <p>Please enter your email address to search for your account.</p>
                            {this.state.error && <Alert color="danger">{this.state.error}</Alert>}
                            {this.state.linkSent && <Alert color="success">Please check your email and click the secure link.</Alert>}
                            <FormGroup>
                            <Input
                                type="email"
                                name="email"
                                placeholder="email" 
                            />
                            </FormGroup>
                            <Button>Send me email</Button>
                            <div style={{textAlign: 'center',padding: '20px',background: '#F2E9E9', marginTop: '20px'}}>It may take several minutes to receive a password reset email. 
                            Make sure to check your junk mail.</div>
                        </Form>
                    </div>
                </Container>
            </div>
        )
    }
}

export default ForgotPassword;