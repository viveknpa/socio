import React from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem
} from "reactstrap";
import { Container, Button, Form, FormGroup, Alert, Input, FormText } from "reactstrap";

class VerifyAccount extends React.Component {
    state = {
        verified: false,
        error: ''
    }
    handleSubmit = (e) => {
        e.preventDefault();
        if(e.target.secretToken.value.trim() < 32) {
            return this.setState({error: 'invalid code'});
        }
        axios.post('/verifyaccount', {
            secretToken: e.target.secretToken.value
        }).then(() => {
            this.setState({error: ''});
            this.setState({verified: true});
        }).catch((e) => {
            this.setState({error: 'invalid code'});
        })
    }
    render() {
        return (
            <div>
            <Navbar light expand="md" style={{ background: '#0747A6' }}>
            <Container>
                <NavbarBrand tag={Link} to="/" style={{ color: 'white' }}>Logo</NavbarBrand>
            </Container>
        </Navbar>
        <Container>
            <div style={{ display: 'flex', justifyContent: 'center', marginTop: '50px' }}>
                <Form style={{ maxWidth: '400px' }} onSubmit={this.handleSubmit}>
                    <h2 style={{fontWeight: '300'}}>Verify your email address.</h2>
                    <p>We have sent you a Security Code to verify your account</p>
                    {this.state.verified && <Alert color="success">Verified, now you may login <Link to="/"> Go back</Link></Alert>}
                    {this.state.error && <Alert color="danger">Invalid Code</Alert>}
                    <FormGroup>
                    <Input
                        type="text" 
                        name="secretToken" 
                        placeholder="paste the Code here"
                    />
                    </FormGroup>
                    <Button>Verify</Button>
                </Form>
            </div>
        </Container>F
            </div>
        )
    }
}

export default VerifyAccount;