import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { Button, Form, FormGroup, Label, Input, FormText, Container, ListGroup, ListGroupItem } from 'reactstrap';
import MapWithControlledZoom from './maps/MapWithControlledZoom';
import axios from 'axios';

class AddStreet extends React.Component {
    state = {
        location: undefined,   //{lat, lng}
        note: '',
        localities: [],
        selectedLocality: '', // {label, value}
    }
    config = {
        headers: { 'x-auth': localStorage.getItem('x-auth') }
    }
    handleCurrentLocation = () => {
        if(!navigator.geolocation) {
            return alert('geolocation not supported by your browser');
          }
        var geoOptions = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        }
        var geoError = (error) => {
            console.log('Error occurred. Error code: ' + error.code);
            // error.code can be:
            //   0: unknown error
            //   1: permission denied
            //   2: position unavailable (error response from location provider)
            //   3: timed out
        };
          navigator.geolocation.getCurrentPosition((position) => {
            this.setState({ 
                location: {
                    lat: position.coords.latitude, 
                    lng: position.coords.longitude
                }
             })
          }, geoError, geoOptions);
    }
    handleNoteChange = (e) => {
        this.setState({ note: e.target.value });
    }
    handleLocalityChange = (selectedOption) => {
        selectedOption && this.setState({ selectedLocality: selectedOption.value });
    }
    onSubmit = (e) => {
        e.preventDefault();
        const { note, location, selectedLocality:locality } = this.state;
        if(!location || !locality) {
            return alert('All fields are required!');
        }
        axios.post('/streets', {note, location, locality}, this.config).then((response) => {
            this.props.addStreet(response.data.street);
            this.props.tabChange('3');
        }).catch((e) => {
            alert('Unable to add street');
            console.log(e);
        })
    }
    componentWillMount() {
        axios.get('/localitylist', this.config).then((response) => {
            this.setState({ localities: response.data.localities })
        }).catch((e) => {
            console.log(e);
        })
	}
    render() {
        const { selectedOption } = this.state;
        return (
            <Container>
                <Form onSubmit={this.onSubmit}><br/>
                    <FormGroup>
                        <Button outline color="info" type="button" onClick={this.handleCurrentLocation}>Get current Location</Button><br/>
                        <br />{this.state.location && <MapWithControlledZoom location={this.state.location}/>}
                    </FormGroup>
                    <FormGroup>
                        <Label for="note">add a note</Label>
                        <Input 
                            type="text" 
                            name="note" 
                            placeholder="Add a note" 
                            value={this.state.note}
                            onChange={this.handleNoteChange}
                            required
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="note">Select Locality</Label>
                        <Select
                            value={this.state.selectedLocality}
                            onChange={this.handleLocalityChange}
                            options={this.state.localities.map((locality) => {
                                return { value: locality, label: locality };
                            })}
                        />
                    </FormGroup>
                    <Button color="warning">Save</Button>
                </Form>
            </Container>
        )
    }
}

export default AddStreet;