import React from 'react';
import {Link} from 'react-router-dom';

class PostListItem extends React.Component {
    render() {
        return (
            <tr>    
                <td data-label="full name">{this.props.fullName}</td>
                <td data-label="total assingned">{this.props.resolved+this.props.rejected+this.props.pending}<br/>{this.props.longitude}</td>
                <td data-label="pending"><Link to={`/dashboard/viewPost/pending/${this.props._id}`}>{this.props.pending}</Link></td> 
                <td data-label="rejolved"><Link to={`/dashboard/viewPost/resolved/${this.props._id}`}>{this.props.resolved}</Link></td> 
                <td data-label="rejected"><Link to={`/dashboard/viewPost/rejected/${this.props._id}`}>{this.props.rejected}</Link></td> 
            </tr>
        )
    }
}

export default PostListItem;