import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import AppRouter, {history} from './routers/AppRouter';
import configureStore from './store/configureStore';
import {login, logout} from './actions/auth';
import './styles/styles.scss';
import 'react-dates/lib/css/_datepicker.css';
import LoadingPage from './components/LoadingPage';
import axios from 'axios';
import {startSetReport} from './actions/report';

export const store = configureStore();

const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
)

let hasRendered = false;
const renderApp = () => {
    if(!hasRendered) {
        ReactDOM.render(jsx, document.getElementById('app'));
        hasRendered = true;
    }
}

ReactDOM.render(<LoadingPage />, document.getElementById('app'));

const onAuthStateChanged = () => {
    axios({
        method:'get',
        url:'/users/me',
        headers: {'x-auth': localStorage.getItem('x-auth')}
      }).then((response) => {
        store.dispatch(login({
            uid: response.data._id,
            fullName: response.data.fullName,
            email: response.data.email,
            userType: response.data.userType
        }))
        store.dispatch(startSetReport());
        require('./actions/socket').initialEvents(store)  //create socket connection and emit initial events
        renderApp();
        if(history.location.pathname === '/') {
            history.push('/dashboard/users');
        }
      }).catch((e) => {
        console.log(e);
        store.dispatch(logout());
        const socket = require('./actions/socket').socket;
        socket && socket.close();  //close socket connection
        renderApp();
        if(!history.location.pathname.includes('/reset')) {
            history.push('/');
        }
      });
}

onAuthStateChanged();
export default onAuthStateChanged;