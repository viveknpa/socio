import io from 'socket.io-client';
export let socket;
import {setBookedAmbulanceInfo, setAvailableAmbulancesInfo, cancelAmbulance} from '../actions/ambulance';

function error(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
}

export const initialEvents = (store) => {
    socket = io('/', {
        query: {token: localStorage.getItem('x-auth')}
    })

    socket.on('connect', () => {
        console.log('connected');
    })

    socket.on('booked ambulance info', (data) => {
        store.dispatch(setBookedAmbulanceInfo(data.bookingInfo));
    })

    socket.on('availale ambulances info', (data) => {
        store.dispatch(setAvailableAmbulancesInfo(data.availableAmbulancesInfo));
    })

    socket.on('ambulace cancelled', () => {
        store.dispatch(cancelAmbulance());
    })

    if(store.getState().auth.userType == 3) {
        if(!navigator.geolocation) {
            return alert('geolocation not supported by your browser');
        }
        var geoOptions = {
            enableHighAccuracy: true
        }
        var geoError = function(error) {
            console.log('Error occurred. Error code: ' + error.code);
            // error.code can be:
            //   0: unknown error
            //   1: permission denied
            //   2: position unavailable (error response from location provider)
            //   3: timed out
          };
        setInterval(function() { 
              navigator.geolocation.getCurrentPosition((position) => {
                console.log(position.coords.latitude, position.coords.longitude);
                socket.emit('location update', {
                    latitude: position.coords.latitude, 
                    longitude: position.coords.longitude
                })
              }, geoError, geoOptions);
         }, 5000);
    }   
}




