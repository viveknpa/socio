export const setOriginLocation = (originLocation) => ({
    type: 'SET_ORIGIN_LOCATION',
    originLocation
})

export const setDestinationLocation = (destinationLocation) => ({
    type: 'SET_DESTINATION_LOCATION',
    destinationLocation
})

export const setBookedAmbulanceInfo = (bookedAmbulanceInfo) => ({
    type: 'SET_BOOKED_AMBULANCE_INFO',
    bookedAmbulanceInfo
})

export const setAvailableAmbulancesInfo = (availableAmbulancesInfo) => ({
    type: 'SET_AVAILABLE_AMBULANCES_INFO',
    availableAmbulancesInfo
})

export const cancelAmbulance = () => ({
    type: 'CANCEL AMBULANCE'
})

