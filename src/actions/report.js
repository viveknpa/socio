import axios from 'axios';

export const setReport = (report) => ({
    type: 'SET_REPORT',
    report
})

export const startSetReport = () => {
    return (dispatch) => {
        const config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }
        return axios('/report', config).then((response) => { 
            dispatch(setReport(response.data.report));
        }).catch((e) => {
            console.log(e);
        })
    }
}