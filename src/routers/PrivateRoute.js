import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import NavForMobile from '../components/NavForMobile';

export const PrivateRoute = ({
    isAuthenticated,
    component:Component,
    ...rest
}) => (
    <div>
        <Route {...rest} component={(props) => (
            isAuthenticated ? (
                <div>
                    <Header />
                    <Sidebar />
                    <NavForMobile /> {/* alt od sidebar */}
                    <Component {...props}/>
                </div>
            ) : (
                <Redirect to="/" />
            )
        )} />
    </div>
)

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.auth.uid
})

export default connect(mapStateToProps)(PrivateRoute);