import React from 'react';
import { Router, Route, Switch} from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import createHistory from 'history/createBrowserHistory'
import DashboardPage from '../components/DashboardPage';
import NotFoundPage from '../components/NotFoundPage';
import LoginPage from '../components/LoginPage';
import VerifyAccount from '../components/VerifyAccount';
import ForgotPassword from '../components/ForgotPassword'; 
import ResetPassword from '../components/ResetPassword';
import AddComplaintPage from '../components/AddComplaintPage';
import PostListPage from '../components/PostListPage';
import ReportListPage from '../components/ReportListPage';
import AmbulancePage from '../components/AmbulancePage';
import StreetViewPage from '../components/StreetViewPage';
import SettingPage from '../components/SettingPage';

export const history = createHistory();

const AppRouter = () => ( 
    <Router history={history}>
        <div>
            <Switch>
                <PublicRoute path="/" component={LoginPage} exact={true}/>
                <PublicRoute path="/forgot" component={ForgotPassword} />
                <Route path="/reset/:secretToken" component={ResetPassword} />
                <PublicRoute path="/verify" component={VerifyAccount}/>
                <PrivateRoute path="/dashboard/users" component={DashboardPage} exact={true}/> 
                <PrivateRoute path="/dashboard/addPost" component={AddComplaintPage} /> 
                <PrivateRoute path="/dashboard/viewPost/" component={PostListPage} exact={true}/> 
                <PrivateRoute path="/dashboard/viewPost/:status/:typeFourUid" component={PostListPage}/> 
                <PrivateRoute path="/dashboard/report" component={ReportListPage} /> 
                <PrivateRoute path="/dashboard/streetView" component={StreetViewPage} /> 
                <PrivateRoute path="/dashboard/ambulance" component={AmbulancePage} /> 
                <PrivateRoute path="/dashboard/setting" component={SettingPage} /> 
                <Route component={NotFoundPage} />
            </Switch>
        </div>
    </Router>
);

export default AppRouter;
