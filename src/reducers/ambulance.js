export default (state = {}, action) => {
    switch(action.type) {
        case 'SET_ORIGIN_LOCATION': 
            return {
                ...state,
                originLocation: action.originLocation
            }
        case 'SET_DESTINATION_LOCATION':
            return {
                ...state,
                destinationLocation: action.destinationLocation
            }
        case 'SET_BOOKED_AMBULANCE_INFO':
            return {
                ...state,
                bookedAmbulanceInfo: action.bookedAmbulanceInfo
            }
        case 'SET_AVAILABLE_AMBULANCES_INFO':
            return {
                ...state,
                availableAmbulancesInfo: action.availableAmbulancesInfo
            }
        case 'CANCEL AMBULANCE':
            return {
                ...state,
                bookedAmbulanceInfo: null,
                availableAmbulancesInfo: null
            }
        default: 
            return state
    }
}