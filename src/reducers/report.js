export default (state = [], action) => {
    switch(action.type) {
        case 'SET_REPORT': 
            return action.report
        default: 
            return state
    }
}