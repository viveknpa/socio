class AmbulanceUsers {
    constructor() {
      this.users = [];
    }
  
    addUser (socketId, uid) {
      let user = this.users.filter((user) => user.uid.toHexString() === uid.toHexString())[0];
      if(user) {
        this.users.map((user) => {
          if(user.uid.toHexString() === uid.toHexString()) {
            user.socketId = socketId;
            user.active = true;
          }
          return user;
        })
      } else {
        const newUser = {socketId, uid, available: true, active: true};
        this.users.push(newUser);
      }
    }

    removeUser(socketId) {
      var user = this.getUser(socketId);
  
      if(user) {
        this.users = this.users.filter((user) => user.socketId !== socketId);
      }
      return user
    }

    getUser (socketId) {
        return this.users.filter((user) => user.socketId === socketId)[0];
    }

    getUsers () {
      return this.users;
    }

    updateUserLocation(socketId, latitude, longitude) {
      let updatedUser;
        this.users = this.users.map((user) => {
            if(user.socketId === socketId) {
                user.latitude = latitude;
                user.longitude = longitude;
                updatedUser = user;
            }
            return user;
        })
        return updatedUser;
    }

    updateUserStatusByUid(uid) {  
      this.users = this.users.map((user) => {
        if(user.uid.toHexString() === uid) {
          user.available = !user.available;
        }
        return user;
      })
    }

    updateUserActiveStatus(socketId) {
      this.users = this.users.map((user) => {
        if(user.socketId === socketId) {
          user.active = false;
        }
        return user;
      })
    }

    updateBookingInfo(socketId, info) {
      this.users = this.users.map((user) => {
        if(user.socketId === socketId) {
          user.available = false;
          user.bookingInfo = info;
        }
        return user;
      })
    }
  }
  
  module.exports = {AmbulanceUsers};

//text data

// {
//   socketId: '1',
//   uid: '5afc173a0e932424787aff09',
//   latitude: 22.5957689,
//   longitude: 88.26363940000002,
//   available: true
// }, {
//   socketId: '2',
//   uid: '5afc173a0e932424787aff09',
//   latitude: 22.6470514,
//   longitude: 88.43168299999999,
//   available: false
// }, {
//   socketId: '3',
//   uid: '5afab24fbb29972abcfaaaab',
//   latitude: 45.5,
//   longitude: 255,
//   available: false
// }, {
//   socketId: '4',
//   uid: '5afab20fbb29972abcfaaaae',
//   latitude: 22.59,
//   longitude: 88.26300002,
//   available: true
// }, {
//   socketId: '5',
//   uid: '5afab24fdc29972abcfaaaab',
//   latitude: 22.5,
//   longitude: 88.6300002,
//   available: false
// }

  
  