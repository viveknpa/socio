const _ = require('lodash');
const {ObjectID} = require('mongodb');
const axios = require('axios');
const {User} = require('./models/user');
const { AmbulanceUsers } = require('./socket-utils/ambulanceUsers');
const ambulanceUsers = new AmbulanceUsers();
const currentUsers = {};

const findUidBySocketId = (socketId) => {
    for (var key in currentUsers) {
        if (currentUsers[key] == socketId) return key; 
    }
}

module.exports = (io) => {
    io.use(function (socket, next) {
        if (socket.handshake.query && socket.handshake.query.token) {
            const token = socket.handshake.query.token;
            User.findByToken(token).then((user) => {
                if (!user) {
                    return Promise.reject();
                }
                currentUsers[user._id] = socket.id;
                user.userType === 3 && ambulanceUsers.addUser(socket.id, user._id);
                next();
            }).catch((e) => {
                console.log(e);
                next(new Error('Authentication error'));
            });
        } else {
            next();
        }
    })

    io.on('connection', (socket) => {
        console.log('New user connected');

        socket.on('location update', ({latitude, longitude}) => {
            let updatedAmbulanceUser = ambulanceUsers.updateUserLocation(socket.id, latitude, longitude);

            if(updatedAmbulanceUser && !updatedAmbulanceUser.available) {
                const toUid = updatedAmbulanceUser.bookingInfo.ambulanceBooker_uid; //get socketid of user to emit event
                const toSocketId = currentUsers[toUid];
                updatedAmbulanceUser.bookingInfo.currentLocation = {
                    lat: updatedAmbulanceUser.latitude,
                    lng: updatedAmbulanceUser.longitude
                }
                io.to(toSocketId).emit('booked ambulance info', { bookingInfo: updatedAmbulanceUser.bookingInfo });
                io.to(updatedAmbulanceUser.socketId).emit('booked ambulance info', { bookingInfo: updatedAmbulanceUser.bookingInfo });

            }
        })

        socket.on('search ambulance', (originLocation, callback) => {
            let url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${originLocation.latitude},${originLocation.longitude}&destinations=`;
            let ambulanceUserLocations = ''; //ambulance users locations(lat, lng) as destinations corresponding to pickup location
            ambulanceUsers.getUsers().map((user, index) => {
                if(user.active && user.available) {
                    ambulanceUserLocations += `${user.latitude},${user.longitude}|`;
                }
            })
            ambulanceUserLocations = ambulanceUserLocations.substring(0, ambulanceUserLocations.length - 1);
            if(ambulanceUserLocations) {
                url += `${ambulanceUserLocations}&key=AIzaSyDVuW9Hzh5YULvxA701HkpDSwv8X6I6ScM`;
                axios.get(url).then((response) => {
                    let availableAmbulancesInfo = [];  // info means total distance and time to reach at pickup location
                    response.data.rows[0].elements.map((element) => {
                        if(element.status == 'OK') {
                            const data = {};
                            data.distance = element.distance.text;
                            data.duration = element.duration.text;
                            availableAmbulancesInfo.push(data)
                        }
                    })
                    if(availableAmbulancesInfo.length) {
                        socket.emit('availale ambulances info', {
                            availableAmbulancesInfo
                        })
                    } else {
                        callback('No ambulance available at this moment!');
                    }
                }).catch((e) => {
                    console.log(e);
                })
            } else {
                callback('No ambulance available at this moment!');
            }  
        })

        socket.on('book ambulance', (location, callback) => {
            const originLocation = location.originLocation;
            let url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${originLocation.latitude},${originLocation.longitude}&destinations=`;
            let ambulanceUserLocations = ''; //ambulance users locations(lat, lng) as destinations corresponding to pickup location
            let rawAmbulanceUsers = ambulanceUsers.getUsers();
            rawAmbulanceUsers.forEach((user, index) => {
                if(user.active && user.available) {
                    ambulanceUserLocations += `${user.latitude},${user.longitude}|`;
                }
            })
            firstFilteredAmbulanceUsers = rawAmbulanceUsers.filter((user) => user.available === true);
            ambulanceUserLocations = ambulanceUserLocations.substring(0, ambulanceUserLocations.length - 1);
            if(ambulanceUserLocations) {
                url += `${ambulanceUserLocations}&key=AIzaSyDVuW9Hzh5YULvxA701HkpDSwv8X6I6ScM`;
                // console.log(url);
                axios.get(url).then((response) => {
                    let amulanceDisances = []; //from pickup location
                    const secondFilteredAmbulanceUsers = [];
                    response.data.rows[0].elements.map((element, index) => {
                        if(element.status == 'OK') {
                            const distanceUnit = element.distance.text.split(" ")[1];
                            let distance;
                            if(distanceUnit == 'km') {
                                distance = parseFloat(element.distance.text.substring(0, element.distance.text.length - 3), 10);
                            } else {
                                distance = parseFloat(element.distance.text.substring(0, element.distance.text.length - 2), 10)/1000;
                            }
                            amulanceDisances.push(distance);
                            secondFilteredAmbulanceUsers.push(firstFilteredAmbulanceUsers[index]);
                        }
                    })
                    const ambulanceAtMinimumDistanceIndex = amulanceDisances.indexOf(Math.min.apply(Math, amulanceDisances));
                    const min = Math.min.apply(Math, amulanceDisances);
                    
                    // console.log(amulanceDisances, amulanceDisances.length, ambulanceAtMinimumDistanceIndex, min);
                    // console.log(secondFilteredAmbulanceUsers);
                    // console.log('selected ', secondFilteredAmbulanceUsers[ambulanceAtMinimumDistanceIndex]);
                    
                    const selectedAmbulance = secondFilteredAmbulanceUsers[ambulanceAtMinimumDistanceIndex];
                    User.find({
                        '_id': { $in: [
                            selectedAmbulance.uid,
                            new ObjectID(findUidBySocketId(socket.id))
                        ]}
                    }).then((users) => {
                        const indexOfAmbulanceUser = _.findIndex(users, ['_id', selectedAmbulance.uid]);
                        const indexOfCurrentUser = _.findIndex(users, ['_id', new ObjectID(findUidBySocketId(socket.id))] );
        
                        const info = {
                            ambulanceUser_uid: selectedAmbulance.uid,
                            ambulanceUser_fullName: users[indexOfAmbulanceUser].fullName,
                            ambulanceUser_mobile: users[indexOfAmbulanceUser].mobile,
                            ambulanceBooker_uid: new ObjectID(findUidBySocketId(socket.id)),
                            ambulanceBooker_fullName: users[indexOfCurrentUser].fullName,
                            ambulanceBooker_mobile: users[indexOfCurrentUser].mobile,
                            origin: {
                                lat: location.originLocation.latitude,
                                lng: location.originLocation.longitude,
                            },
                            destination: {
                                lat: location.destinationLocation.latitude,
                                lng: location.destinationLocation.longitude,
                            }
                        }
                        ambulanceUsers.updateBookingInfo(selectedAmbulance.socketId, info);
                        
                        socket.emit('booked ambulance info', { bookingInfo: info });
                        io.to(currentUsers[selectedAmbulance.uid]).emit('booked ambulance info', { bookingInfo: info });

                    })
                }).catch((e) => {
                    console.log(e);
                })
            } else {
                callback('No ambulance available at this moment!');
            }  
        })

        socket.on('cancel ambulance', (data) => {
            ambulanceUsers.updateUserStatusByUid(data.uid);
            socket.emit('ambulace cancelled');
            io.to(currentUsers[data.uid]).emit('ambulace cancelled');
        })

        socket.on('disconnect', () => {
           console.log('user disconnected!');
           delete currentUsers[findUidBySocketId(socket.id)]; 
           ambulanceUsers.updateUserActiveStatus(socket.id);
        });
    });
}


