var mongoose = require('mongoose');

var streetComplaintSchema = new mongoose.Schema({ 
    subject: String,
    streetNumber: String,
    createdAt: {
        type: Date,
        default: Date.now
    },
    _creator: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    creatorName: String
})

var StreetComplaint = mongoose.model('StreetComplaint', streetComplaintSchema);

module.exports = {StreetComplaint};