var mongoose = require('mongoose');

var StreetSchema = new mongoose.Schema({ 
    note: { type: String, trim: true },
    location: { 
        lat: {
            type: Number,
        },
        lng: {
            type: Number
        }
     },
     locality: String,
     streetNumber: String
})

var Street = mongoose.model('Street', StreetSchema);

module.exports = {Street};