var mongoose = require('mongoose');

var HeadLocalitySchema = new mongoose.Schema({ 
    state: String,
    district: String
})

var HeadLocality = mongoose.model('HeadLocality', HeadLocalitySchema);

module.exports = { HeadLocality };