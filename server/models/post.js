var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({ 
    subject: String,
    images: [String],
    latitude: Number,
    longitude: Number,
    createdAt: {
        type: Date,
        default: Date.now
    },
    _creator: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
      },
    creatorName: String,
    assignTo: mongoose.Schema.Types.ObjectId,
    status: { type: String, default: 'pending' }
})

var Post = mongoose.model('Post', PostSchema);

module.exports = {Post};