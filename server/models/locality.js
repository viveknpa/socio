var mongoose = require('mongoose');

var LocalitySchema = new mongoose.Schema({ 
    locality: String,
    office: String,
    pincode: Number,
    'sub-district': String,
    district: String,
    state: String
})

var Locality = mongoose.model('Locality', LocalitySchema);

module.exports = {Locality};