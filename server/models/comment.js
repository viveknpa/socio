const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
    postId: mongoose.Schema.Types.ObjectId,
    commentedBy_uid: mongoose.Schema.Types.ObjectId,
    commentedBy_fullName: String,
    text: { type: String, trim: true, minlength: 1 },
    commentedAt: Number,
    reply: [{
        repliedAt: String,
        repliedBy_fullName: { type: String, trim: true, minlength: 1 },
        repliedBy_uid: mongoose.Schema.Types.ObjectId,
        text: String
    }]
})

var Comment = mongoose.model('Comment', CommentSchema);

module.exports = { Comment };