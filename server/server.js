require('./config/config');

const path = require('path');
const _ = require('lodash');
var express = require('express');
var bodyParser = require('body-parser');
var randomstring = require("randomstring");
const converter = require('number-to-words');
var { ObjectID } = require('mongodb');

var { mongoose } = require('./db/mongoose');
var { User } = require('./models/user');
var { Post } = require('./models/post');
var { StreetComplaint } = require('./models/streetComplaint');
var { Comment } = require('./models/comment');
var { UserType } = require('./models/userType');
var { Locality } = require('./models/locality');
var { HeadLocality } = require('./models/headLocality');
var { Street } = require('./models/street');
var { authenticate } = require('./middleware/authenticate');
var { sendVerifyMail } = require('./misc/sendgrid');
var { upload } = require('./misc/multer');
var { getDistanceFromLatLonInM } = require('./misc/locationDistance');

var app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = process.env.PORT;
const publicPath = path.join(__dirname, '..', 'public');
app.use(express.static(publicPath));
app.use(bodyParser.json());
require('./socket')(io);

// upload cleanliness complaint
app.post('/post', authenticate, (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      res.status(400).send(err);
    } else {
      if (req.files == undefined) {
        res.status(400).send('Error: No File Selected!');
      } else {
        const images = req.files.map((file) => file.filename);
        const { subject, latitude, longitude } = req.body;
        new Post({
          _creator: req.user._id,
          creatorName: req.user.fullName,
          subject, latitude, longitude, images
        }).save().then((post) => {
          res.status(200).send({ post });
        }).catch((e) => {
          console.log(e);
          res.status(400).send();
        })
      }
    }
  });
});

//new comment on a post
app.post('/post/comment/:id', authenticate, (req, res) => {
  const postId = req.params.id;
  const body = _.pick(req.body, ['text']);
  body.postId = postId;
  body.commentedBy_fullName = req.user.fullName;
  body.commentedBy_uid = req.user._id;
  body.commentedAt = new Date().getTime();
  new Comment(body).save().then((comment) => {
    res.send({ comment });
  }).catch((e) => {
    console.log(e);
    res.status(400).send(e);
  })
})

//reply on a comment 
app.post('/post/comment/reply/:id', authenticate, (req, res) => {
  const commentId = req.params.id;
  const body = _.pick(req.body, ['text']);
  body.repliedBy_fullName = req.user.fullName;
  body.repliedBy_uid = req.user._id;
  body.repliedAt = new Date().getTime();
  Comment.findOne({ _id: commentId }).then((comment) => {
    return comment.update({
      $push: { reply: body }
    })
  }).then(() => {
    res.send({ reply: body });
  }).catch((e) => {
    res.status((e) => {
      res.status(400).send(e);
    })
  })
})

// get list of all comments related to a post
app.get('/post/comment/:id', authenticate, (req, res) => {
  const postId = req.params.id;
  Comment.find({ postId }).then((comments) => {
    res.send({ comments });
  }).catch((e) => {
    res.status(400).send(e);
  })
})

//get all cleanliness complaints 
app.get('/post', authenticate, (req, res) => {
  let query;
  const _id = req.user._id;
  switch (req.user.userType) {
    case 1:
      query = {}
      break
    case 4:
      query = { assignTo: _id }
      break
    default:
      query = { _creator: _id }
  }
  Post.find(query).then((posts) => {
    res.send({ posts });
  }).catch((e) => {
    console.log(e);
    res.status(400).send();
  })
})

// add street complaint
app.post('/post/street', authenticate, (req, res) => {
  const { subject, streetNumber } =  req.body;
  new StreetComplaint({
    _creator: req.user._id,
    creatorName: req.user.fullName,
    subject, streetNumber
  }).save().then((post) => {
    res.send({ post });
  }).catch((e) => {
    console.log(e);
    res.status(400).send();
  })
})

// add street complaint
app.get('/post/street', authenticate, (req, res) => {
  StreetComplaint.find().then((complaints) => {
    res.send({ complaints });
  }).catch((e) => {
    console.log(e);
    res.status(400).send();
  })
})

// match with street locations
app.post('/post/street/match', (req, res) => {
  const { lat: lat1, lng: lng1 } = req.body;
  Street.find().then((streets) => {
    streets.forEach((street) => {
      const lat2 = street.location.lat;
      const lng2 = street.location.lng;
      console.log(getDistanceFromLatLonInM(lat1, lng1, lat2, lng2))
      if(getDistanceFromLatLonInM(lat1, lng1, lat2, lng2) <= 14) {
        return res.send({ 
          streetNumber: street.streetNumber,
          location: street.location
        });
      }
    })
    res.status(404).send();
  }).catch((e) => {
    res.status(400).send();
    console.log(e);
  })
})

//SignUp route
app.post('/users', (req, res) => {
  var body = _.pick(req.body, ['fullName', 'email', 'mobile', 'password']);
  body.secretToken = randomstring.generate(); //to verify email
  body.active = false;
  var user = new User(body);

  const to = body.email;
  const text = `copy and paste the following code: ${body.secretToken}`;
  const subject = 'Verify your account on npaGuru';

  sendVerifyMail(to, subject, text);

  user.save().then((user) => {
    res.status(200).send();
  }).catch((e) => {
    console.log(e);
    res.status(400).send();
  });
});

//verify account by sending an email with randomString
app.post('/verifyaccount', (req, res) => {
  const secretToken = req.body.secretToken.trim();
  User.findOne({ secretToken }).then((user) => {
    if (!user) {
      return res.status(404).send();
    }
    return user.update({
      $set: {
        secretToken: '',
        active: true
      }
    })
  }).then(() => {
    res.status(200).send();
  }).catch((e) => {
    res.status(400).send();
  })
})

//forgot password
app.post('/forgot', (req, res) => {
  User.findOne({ email: req.body.email }).then((user) => {
    if (!user) {
      return res.status(404).send();
    }
    const secretToken = randomstring.generate();

    const to = req.body.email;
    const subject = 'reset password on npaGuru';
    const text = 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
      'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
      'http://' + req.headers.host + '/reset/' + secretToken + '\n\n' +
      'If you did not request this, please ignore this email and your password will remain unchanged.\n';

    sendVerifyMail(to, subject, text);
    return user.update({
      $set: {
        secretToken
      }
    })
  }).then(() => {
    res.status(200).send();
  }).catch((e) => {
    console.log(e);
    res.status(400).send();
  })
});

//reset password 
app.post('/reset/:secretToken', (req, res) => {
  const secretToken = req.params.secretToken;
  User.findOne({ secretToken }).then((user) => {
    if (!user) {
      return res.status(404).send();
    }
    user.password = req.body.password;
    user.secretToken = '';
    return user.save();
  }).then(() => {
    res.status(200).send();
  }).catch((e) => {
    console.log(e);
    res.status(400).send();
  })
})

//reset password for authenticated user
app.post('/reset', authenticate, (req, res) => {
  User.findOne({ _id: req.user._id }).then((user) => {
    if (!user) {
      return res.status(404).send();
    }
    user.password = req.body.password;
    return user.save();
  }).then(() => {
    res.status(200).send();
  }).catch((e) => {
    console.log(e);
    res.status(400).send();
  })
})

//users update
app.post('/users/userupdate', authenticate, (req, res) => {
  const { id, active, userType } = req.body;
  if (req.user.userType != 1) {
    return res.status(400).send('Error: Only Admin can update a user');
  }
  User.findById(id).then((user) => {
    if (!user) {
      return res.status(400).send();
    }
    return user.update({
      $set: {
        active,
        userType
      }
    })
  }).then(() => {
    res.status(200).send();
  }).catch((e) => {
    res.status(400).send();
  })
})

//this route will return a individual authenticated user
app.get('/users/me', authenticate, (req, res) => {
  res.send(req.user);
});

app.get('/getUsersList', authenticate, (req, res) => {
  User.find().then((users) => {
    res.send({ users });
  }).catch((e) => {
    console.log(e);
    res.status(400).send();
  })
})

//admin request to get list of typeFour users to assign the posts
app.get('/users/typeFour', authenticate, (req, res) => {
  User.findById(req.user._id).then((user) => {
    if (user.userType !== 1) {
      return res.status(400).send('Error: Only admin has permission to get list of typeFour users')
    }
    return User.find({ userType: 4 });
  }).then((users) => {
    res.send({ users });
  }).catch((e) => {
    console.log(e);
  })
})

//admin request to assign a post to a specific user
app.post('/post/assignto', authenticate, (req, res) => {
  if (req.user.userType !== 1) {
    return res.status(400).send('Error: Only admin has permission to assign post')
  }
  const { postId, assignTo } = req.body;

  Post.findById(postId).then((post) => {
    if (!post) {
      return res.status(404).send('post not found');
    }
    return post.update({
      $set: { assignTo }
    })
  }).then((post) => {
    res.status(200).send();
  }).catch((e) => {
    console.log(e);
    res.status(400).send();
  })
})

//admin to create a new user Type 
app.post('/usertype/create', (req, res) => {
  const body = _.pick(req.body, ['displayName', 'description']);
  UserType.find().distinct('type').then((types) => {
    let newType;
    if(types.length) {
      newType = Math.max(...types) + 1;
    } else {
      newType = 1
    }
    body.type = newType;
    new UserType(body).save().then((doc) => {
      res.send({userType: doc});
    })
  }).catch((e) => {
    res.status(400).send(e);
  })
})

//admin to remove a user Type 
app.delete('/usertype/remove/:id', authenticate, (req, res) => {
  const id = req.params.id;
  if(!ObjectID.isValid(id)){
    return res.status(404).send();
  }
  UserType.findByIdAndRemove(id).then((userType) => {
    if(!userType){
      return res.status(404).send({ userType });
    }
    User.update({ userType: userType.type }, { userType: 5 }, { multi: true }).then((data) => {
      console.log(data);
    })
    res.status(200).send();
  }).catch((e) => {
    res.status(400).send();
    console.log(e);
  })
})

// edit userType to add displayName and description
app.patch('/userType/edit/:id', (req, res) => {
  const id = req.params.id;
  const body = _.pick(req.body, ['description', 'displayName', 'authority']);
  UserType.findOneAndUpdate({_id: id}, {$set: body}, { new: true }).then((type) => {
    res.send({body, userType: type});
  }).catch((e) => {
    console.log(e);
    res.status(400).send(e);
  })
})

//admin to get list of userTypes as array
app.get('/usertype/list', (req, res) => {
  UserType.find().then((types) => {
    res.send({ userTypes: types });
  }).catch((e) => {
    res.status(400).send(e);
  })
})

//type four user to update the status of a post 
app.post('/post/status', authenticate, (req, res) => {
  if (req.user.userType !== 'typeFour') {
    return res.status(400).send('Error: Only type four user has permission to change the status of a  post')
  }
  const { postId, status } = req.body;

  Post.findById(postId).then((post) => {
    if (!post) {
      return res.status(404).send('post not found');
    }
    return post.update({
      $set: { status }
    })
  }).then((post) => {
    res.status(200).send();
  }).catch((e) => {
    console.log(e);
    res.status(400).send();
  })
})

//get report document only for typeOne and typeFour users
app.get('/report', authenticate, (req, res) => {
  let query;
  switch (req.user.userType) {
    case 1:
      query = { userType: 4 }
      break
    case 4:
      query = { _id: req.user._id }
      break
    default:
      return res.status(400).send('Error: Only type four and type one user has permission to get posts report')
  }
  User.find(query).then((users) => {
    const report = [];
    users.forEach((user, index) => {
      var reportItem = {};
      Post.find({
        assignTo: user._id
      }).then((result) => {
        const pending = result.filter(one => one.status === 'pending');
        reportItem.pending = pending.length;
        reportItem.pendingPosts = pending;
        const resolved = result.filter(one => one.status === 'resolved');
        reportItem.resolved = resolved.length;
        reportItem.resolvedPosts = resolved;
        const rejected = result.filter(one => one.status === 'rejected');
        reportItem.rejected = rejected.length;
        reportItem.rejectedPosts = rejected;
        reportItem._id = user._id;
        reportItem.fullName = user.fullName;
        report.push(reportItem);
        if (report.length == users.length) {
          res.send({ report });
        }
      })
    })
  })
})

// POST /users/login {email, password}
app.post('/users/login', (req, res) => {
  var body = _.pick(req.body, ['email', 'password']);

  User.findByCredentials(body.email, body.password).then((user) => {
    return user.generateAuthToken().then((token) => {
      res.header('x-auth', token).send(user);
    });
  }).catch((e) => {
    console.log(e);
    res.status(400).send(e);
  });
});

app.delete('/users/me/token', authenticate, (req, res) => {
  req.user.removeToken(req.token).then(() => {
    res.status(200).send();
  }, () => {
    res.status(400).send();
  });
});

//return an array of state list
app.get('/statelist', (req, res) => {         
  Locality.find().distinct('state').then((states) => {
    res.send({states});
  }).catch((e) => {
    res.status(400).send(e);
  })
})

//return  an array of district list
app.post('/districtlist', (req, res) => {     
  const state = req.body.state;
  Locality.find({ state }).distinct('district').then((districts) => {
    res.send({districts});
  }).catch((e) => {
    res.status(400).send(e);
  })
})

//return an array of locality list
app.get('/localitylist', (req, res) => {
  HeadLocality.find().then((docs) => {
    const {state, district} = docs[0];
    return Locality.find({ state, district }).distinct('locality')
  }).then((list) => {
    res.send({ localities: list});
  }).catch((e) => {
    res.status(400).send(e);
  })
})

// add state and city by admin
app.post('/headlocality', (req, res) => {
  const { state, district } = req.body;
  HeadLocality.findOneAndRemove({}).then(() => {
    return new HeadLocality({ state, district }).save();
  }).then((headLocality) => {
    res.send({ headLocality })
  }).catch((e) => {
    res.status(400).send();
    console.log(e);
  })
})


// get {state, city} by admin
app.get('/headlocality', (req, res) => {
  HeadLocality.find().then((docs) => {
    console.log(docs);
    res.send({headLocality: docs[0]});
  }).catch((e) => {
    res.status(400).send();
    console.log(e);
  })
})

//add street by street admin
app.post('/streets', (req, res) => {
  const { location, note, locality } = req.body;
  Street.count({}).then((count) => {
      const streetNumber = `${locality}-${count + 1}`   //assuming no complaints are deleted
      return new Street({ location, note, locality, streetNumber }).save();
  }).then((street) => {
      res.send({ street });
  }).catch((e) => {
      console.log(e);
      res.status(400).send(e);
  })
})

//get all added streets
app.get('/streets', (req, res) => {
  Street.find().then((streets) => {
    res.send({ streets });
  }).catch((e) => {
    res.status(400).send();
    console.log(e);
  })
})


app.get('*', (req, res) => {
  res.sendFile(path.join(publicPath, 'index.html'))
})

server.listen(port, () => {
  console.log(`started up at port ${port}`);
});

module.exports = { app };
